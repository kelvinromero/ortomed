from django import template
from home.models import *

register = template.Library()

@register.inclusion_tag('home/tags/categories_filter_dropdown.html', takes_context=True)
def categories_filter_dropdown(context):
    return {
        'categories': ProductCategory.objects.all(),
        'request': context['request'],
    }