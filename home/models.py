# -*- coding: utf-8 -*-
# Imports
from __future__ import absolute_import, unicode_literals
from wagtail.wagtailsearch import index
from django import forms


# Database related
from django.db import models
from modelcluster.fields import ParentalKey, ParentalManyToManyField
from wagtail.wagtailsnippets.models import register_snippet

# Tagging
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase

# Model and field related
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailforms.models import AbstractEmailForm, AbstractFormField

# Admin Interface related
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, FieldRowPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel


# INSTITUCIONAL


class HomePage(Page):
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
    ]


class AboutPage(Page):
    pass


# FORMS

class ContactPage(Page):
    parent_page_types = ['home.HomePage']
    subpage_types = ['home.FormPage']


class FormField(AbstractFormField):
    page = ParentalKey('FormPage', related_name='form_fields')


class FormPage(AbstractEmailForm):
    intro = RichTextField(blank=True)
    thank_you_text = RichTextField(blank=True)

    content_panels = AbstractEmailForm.content_panels + [
        FieldPanel('intro', classname="full"),
        InlinePanel('form_fields', label="Form fields"),
        FieldPanel('thank_you_text', classname="full"),
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel('from_address', classname="col6"),
                FieldPanel('to_address', classname="col6"),
            ]),
            FieldPanel('subject'),
        ], "Email"),
    ]

    parent_page_types = ['home.ContactPage']
    subpage_types = []

# PRODUTOS


class ProductIndexPage(Page):
    """
        Página para listar produtos.
    """
    # Fields
    intro = RichTextField(blank=True)

    # Editor Panels configuration
    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]

    # Parent page / subpage configuration
    parent_page_types = ['home.HomePage']
    subpage_types = ['home.ProductPage']


class ProductPageTag(TaggedItemBase):
    content_object = ParentalKey('ProductPage', related_name='tagged_items')


class ProductPage(Page):
    """
        Página para exibir detalhes do produto.
    """
    body = RichTextField(blank=True)
    tags = ClusterTaggableManager(through=ProductPageTag, blank=True)
    categories = ParentalManyToManyField('home.ProductCategory', blank=True)

    def main_image(self):
        gallery_item = self.gallery_images.first()
        if gallery_item:
            return gallery_item.image
        else:
            return None

    # Indexing fields
    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]

    # Editor Panels configuration
    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('tags'),
            FieldPanel('categories', widget=forms.CheckboxSelectMultiple),
        ], heading="Informações do produto"),
        FieldPanel('body', classname="full"),
        InlinePanel('gallery_images', label="Gallery images"),

    ]

    # Parent page / subpage configuration
    parent_page_types = ['home.ProductIndexPage']
    subpage_types = []


class ProductPageGalleryImage(Orderable):
    page = ParentalKey(ProductPage, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank=True, max_length=250)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]


@register_snippet
class ProductCategory(models.Model):
    name = models.CharField(max_length=255,
                            default="")

    panels = [
        FieldPanel('name'),
    ]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Categoria de produtos"
        verbose_name_plural = 'Categorias de produtos'
